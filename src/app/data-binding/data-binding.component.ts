import { Component, OnInit, NgModule } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css'],
  inputs: ['valorSalvo', 'valorAtual',]
})
export class DataBindingComponent implements OnInit {

  url: string = 'http//loiane.com';
  urlImagem = 'http://lorempixel.com/400/200/sports/';
  cursoAngular: boolean = false;
  valorAtual: string = '';
  valorSalvo = '';

  isMouseOver: boolean = false;

  nomeDoCurso: string = 'Angular';

  nome: string = 'abc';

  pessoa: any = {
    nome: 'Patryck',
    idade: 23
  }

  valorInicial = 0;

  onKeyUp(evento: KeyboardEvent) {
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valor) {
    this.valorSalvo = valor;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  getValor() {
    return 1;
  }

  getCurtirCurso() {
    return true;
  }

  botaoClicado() {
    alert('Botão Clicado!');
  }

  onMudouValor(evento){
    console.log(evento.novoValor);
  }


  constructor() { }

  ngOnInit(): void {
  }

}
